package com.example.musicshop2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    int plus2 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //spinner.findViewById(R.id.spinner);
        //spinnerArrayList = new ArrayList();
        //spinnerArrayList.add("ударные");
        //spinnerArrayList.add("микрофоны");
        //spinnerArrayList.add("Музыкальная клавиатура");

        //spinnerAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, spinnerArrayList);
        //spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        //spinner.setAdapter(spinnerAdapter);
        // Получаем экземпляр элемента Spinner

        Spinner spinner = findViewById(R.id.spinner);

// Настраиваем адаптер
        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(this, R.array.animals,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

// Вызываем адаптер
        spinner.setAdapter(adapter);
    }

    public void minus1(View view) {
        plus2 = plus2 - 1;
        if (plus2 <= 0) {
            plus2 = 0;
        }
        TextView plus1 = findViewById(R.id.textView2);
        plus1.setText("" + plus2);
    }

    public void Plus1(View view) {

        plus2 = plus2 + 1;
        if (plus2 >= 20) {
            plus2 = 20;
        }
        else if (plus2 == 0){
            plus2 = 0;
        }
        TextView plus1 = findViewById(R.id.textView2);
        plus1.setText("" + plus2);
    }

}